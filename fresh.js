const http = require('http')
const fs = require('fs')
const url = 'http://jservice.io/api/clues/?category='

// get is a simple wrapper for request()
// which sets the http method to GET
for (let i = 1; i < 100; i++) {
    var request = http.get(url + 1, function (response) {
        // data is streamed in chunks from the server
        // so we have to handle the "data" event    
        var buffer = "", data


        response.on("data", function (chunk) {
            buffer += chunk;
        });


        response.on("end", function (err) {
            // finished transferring data
            // dump the raw data
            console.log(buffer);
            console.log("\n");
            data = JSON.parse(buffer);

            fs.writeFileSync('categories.json', JSON.stringify(data))
        });

    });
}